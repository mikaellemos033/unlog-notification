package com.br.unlog.notification.infra.repository

import com.br.unlog.notification.domain.model.Template
import org.springframework.data.mongodb.repository.MongoRepository

interface TemplateRepository : MongoRepository<Template, String> {
}