package com.br.unlog.notification

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class UnlogNotificationApplication

fun main(args: Array<String>) {
	runApplication<UnlogNotificationApplication>(*args)
}
