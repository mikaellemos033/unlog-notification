package com.br.unlog.notification.domain.model

import org.springframework.data.mongodb.core.mapping.Document

@Document("templates")
data class Template(
        val id: String,
        val body: String
)
