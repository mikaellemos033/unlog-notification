package com.br.unlog.notification.domain.objectValue

data class Notification(
        val destiny: String,
        val channel: ChannelNotification,
        val subject: String,
        val template: String,
        val params: Map<String, String>
)