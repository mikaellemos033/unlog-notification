package com.br.unlog.notification.domain.objectValue

enum class ChannelNotification {
    SMS, EMAIL;
}