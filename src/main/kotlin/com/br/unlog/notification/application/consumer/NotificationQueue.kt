package com.br.unlog.notification.application.consumer

import com.br.unlog.notification.application.service.NotificationService
import com.br.unlog.notification.domain.objectValue.Notification
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class NotificationQueue(private val notificationService: NotificationService) {

    private val log: Logger = LoggerFactory.getLogger(NotificationQueue::class.java)

    fun queue(notification: Notification) {
        log.info("RECEIVE NOTIFICATION FROM QUEUE TEMPLATE: {}, CHANNEL: {}", notification.template, notification.channel)
        notificationService.notify(notification)
    }
}