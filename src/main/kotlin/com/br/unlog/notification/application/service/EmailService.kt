package com.br.unlog.notification.application.service

import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import com.amazonaws.services.simpleemail.model.*
import com.br.unlog.notification.domain.objectValue.Notification
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class EmailService(
        private val ses: AmazonSimpleEmailService,
        private val service: TemplateService,
        @Value("\${notification.email.from}") private val emailFrom: String,
        @Value("\${notification.email.configSet}") private val configSet: String
) : ChannelNotification {

    private val log: Logger = LoggerFactory.getLogger(EmailService::class.java)

    override fun send(notification: Notification) {

        log.info("Send EMAIL: Template: {}, Destiny: {}", notification.template, notification.destiny)

        val template = service.show(notification.template)
        val subject = Content().withData(notification.subject).withCharset("UTF-8")
        val body = TemplateBindService.parseTemplate(template.body, notification.params)
        val emailBody = Body().withHtml(
                Content().withData(body)
                        .withCharset("UTF-8")
        )

        val request = SendEmailRequest().withSource(emailFrom)
                .withConfigurationSetName(configSet)
                .withDestination(
                        Destination().withToAddresses(notification.destiny)
                )
                .withMessage(
                        Message().withBody(emailBody).withSubject(subject)
                )

        ses.sendEmail(request)
    }
}