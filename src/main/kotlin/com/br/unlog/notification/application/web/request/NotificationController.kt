package com.br.unlog.notification.application.web.request

import com.br.unlog.notification.application.service.NotificationService
import com.br.unlog.notification.domain.objectValue.Notification
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/notification")
class NotificationController(private val notificationService: NotificationService) {

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    fun sendSms(@RequestBody notification: Notification) = notificationService.notify(notification)
}