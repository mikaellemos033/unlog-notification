package com.br.unlog.notification.application.service

import com.br.unlog.notification.domain.objectValue.Notification
import com.br.unlog.notification.domain.objectValue.ChannelNotification
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class NotificationService(
        private val smsService: SMSService,
        private val emailService: EmailService
) {

    private val log: Logger = LoggerFactory.getLogger(NotificationService::class.java)

    fun notify(notification: Notification) {
        log.info("NOTIFY TEMPLATE: {}, CHANNEL: {}", notification.template, notification.channel)

        when(notification.channel) {
            ChannelNotification.SMS -> smsService.send(notification)
            else -> emailService.send(notification)
        }
    }
}