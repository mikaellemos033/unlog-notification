package com.br.unlog.notification.application.service

import com.br.unlog.notification.domain.objectValue.Notification

interface ChannelNotification {
    fun send(notification: Notification)
}