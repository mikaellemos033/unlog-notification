package com.br.unlog.notification.application.web.request

data class TemplateUpdateRequest(val template: String)