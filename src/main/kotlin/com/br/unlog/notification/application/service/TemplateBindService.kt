package com.br.unlog.notification.application.service

class TemplateBindService {
    companion object {
        fun parseTemplate(template: String, params: Map<String, String>): String {
            var body = template

            params.forEach { key, value ->
                body = body.replace("\${$key}", value, true)
            }

            return body
        }
    }
}