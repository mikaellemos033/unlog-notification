package com.br.unlog.notification.application.web

import com.br.unlog.notification.application.service.EmailService
import com.br.unlog.notification.application.service.SMSService
import com.br.unlog.notification.application.service.TemplateService
import com.br.unlog.notification.application.web.request.TemplateUpdateRequest
import com.br.unlog.notification.domain.model.Template
import com.br.unlog.notification.domain.objectValue.Notification
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PageableDefault
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/template")
class TemplateController(private val templateService: TemplateService, private val smsService: EmailService) {
    @GetMapping
    fun list(@PageableDefault() pageable: Pageable) = templateService.list(pageable)

    @GetMapping("/{id}")
    fun show(@PathVariable("id") id: String) = templateService.show(id)

    @PutMapping("/{id}")
    fun update(@PathVariable("id") id: String, @RequestBody templateUpdateRequest: TemplateUpdateRequest) = templateService.update(id, templateUpdateRequest.template)

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody template: Template) = templateService.create(template.copy(id = template.id.toUpperCase()))

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun destroy(@PathVariable("id") id: String) = templateService.delete(id)

    @PostMapping("/send-sms")
    fun sendSms(@RequestBody notification: Notification) {
        smsService.send(notification)
    }
}