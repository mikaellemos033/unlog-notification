package com.br.unlog.notification.application.config

import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Regions
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder
import com.amazonaws.services.sns.AmazonSNS
import com.amazonaws.services.sns.AmazonSNSClient
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class SNSConfig(
        @Value("\${aws.credential.secret}") private val awsSecret: String,
        @Value("\${aws.credential.key}") private val awsKey: String
) {

    @Bean
    fun snsClient(): AmazonSNS? {
        return AmazonSNSClient.builder()
                .withRegion(Regions.US_EAST_1)
                .withCredentials(awsCredentials(awsKey, awsSecret))
                .build()
    }

    @Bean
    fun sesClient(): AmazonSimpleEmailService {
        return AmazonSimpleEmailServiceClientBuilder.standard()
                .withRegion(Regions.US_EAST_1)
                .withCredentials(awsCredentials(awsKey, awsSecret))
                .build()
    }

    private fun awsCredentials(key: String, secret: String): AWSCredentialsProvider {
        val awsCredentials = BasicAWSCredentials(key, secret)
        return AWSStaticCredentialsProvider(awsCredentials)
    }
}