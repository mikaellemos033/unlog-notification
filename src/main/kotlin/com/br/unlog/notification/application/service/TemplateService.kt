package com.br.unlog.notification.application.service

import com.br.unlog.notification.domain.model.Template
import com.br.unlog.notification.infra.repository.TemplateRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.lang.RuntimeException

@Service
class TemplateService(private val templateRepository: TemplateRepository) {

    val log: Logger = LoggerFactory.getLogger(TemplateService::class.java)

    fun list(pageable: Pageable): Page<Template> {
        log.info("list templates")
        return templateRepository.findAll(pageable)
    }

    fun create(template: Template): Template  {
        log.info("create template: {}", template)
        return templateRepository.save(template.copy(id = template.id))
    }

    fun update(id: String, body: String) {
        log.info("update template: id: {}", id)
        val template = templateRepository.findById(id).orElseThrow { RuntimeException() }

        templateRepository.save(template.copy(body = body))
    }

    fun delete(id: String) {
        log.info("delete template: id: {}", id)
        templateRepository.deleteById(id)
    }

    fun show(id: String): Template {
        log.info("delete template: id: {}", id)
        return templateRepository.findById(id).orElseThrow { RuntimeException() }
    }
}