package com.br.unlog.notification.application.service

import com.amazonaws.services.sns.AmazonSNS
import com.amazonaws.services.sns.model.PublishRequest
import com.br.unlog.notification.domain.objectValue.Notification
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class SMSService(
        private val sns: AmazonSNS,
        private val service: TemplateService
) : ChannelNotification {

    private val log: Logger = LoggerFactory.getLogger(SMSService::class.java)

    override fun send(notification: Notification) {
        val template = service.show(notification.template)
        val body = TemplateBindService.parseTemplate(template.body, notification.params)
        val publishRequest = PublishRequest()

        log.info("Send SMS: Template: {}, Destiny: {}", template.id, notification.destiny)

        publishRequest.message = body
        publishRequest.phoneNumber = notification.destiny

        sns.publish(publishRequest)
    }
}